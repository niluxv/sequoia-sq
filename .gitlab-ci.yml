stages:
  - pre-check
  - build
  - test

# These stanzas do some common management tasks before and after the
# job-specific before_script and after_script stanzas are run.
# before_script_start configures any default global state.  The
# job-specific before_script can override this state, if required.
# before_script_end prints out information about the environment to
# improve debugging; it does not modify the environment.
# after_script_end does some common management tasks after the
# job-specific after_script is run.  It prints information about the
# environment, and does some clean up.
#
# Add this to your stanza as follows:
#
#   before_script:
#     - *before_script_start
#     - *** YOUR CODE HERE ***
#     - *before_script_end
#   after_script:
#     - *** YOUR CODE HERE ***
#     - *after_script_end

.before_script_start: &before_script_start
  - 'if test "x${RUSTFLAGS+SET}" = xSET; then echo "\$RUSTFLAGS is set ($RUSTFLAGS)"; exit 1; fi'

.before_script_end: &before_script_end
  - 'if test "x${RUSTFLAGS+SET}" = xSET; then echo "WARNING: before_script set \$RUSTFLAGS ($RUSTFLAGS)"; fi'
  - rustc --version --verbose
  - cargo --version
  - clang -v
  - if [ -d $CARGO_TARGET_DIR ]; then find $CARGO_TARGET_DIR | wc --lines; du -sh $CARGO_TARGET_DIR; fi
  - if [ -d $CARGO_HOME ]; then find $CARGO_HOME | wc --lines; du -sh $CARGO_HOME; fi

.after_script_end: &after_script_end
  - if [ -d $CARGO_TARGET_DIR ]; then find $CARGO_TARGET_DIR -type f -atime +7 -delete; fi
  - if [ -d $CARGO_TARGET_DIR ]; then du -sh $CARGO_TARGET_DIR; fi
  - if [ -d $CARGO_HOME ]; then du -sh $CARGO_HOME; fi

before_script:
  - *before_script_start
  - *before_script_end

after_script:
  - *after_script_end

bookworm:
  tags:
    - linux
  stage: build
  interruptible: true
  image: 192.168.122.1:5000/sequoia-pgp/build-docker-image/bookworm:latest
  script:
    - cargo test --features=subplot
    - if ! git diff --quiet Cargo.lock ; then echo "Cargo.lock changed.  Please add the change to the corresponding commit." ; git diff ; false ; fi
    - if ! git diff --quiet ; then echo "The build changed the source.  Please investigate." ; git diff ; fi
  variables:
    CARGO_TARGET_DIR: /target
    CARGO_HOME: /cargo

sq-features:
  tags:
    - linux
  stage: build
  interruptible: true
  image: 192.168.122.1:5000/sequoia-pgp/build-docker-image/bookworm:latest
  only:
    refs:
      - tags
      - web
      - schedules
  parallel:
    matrix:
      - FEATURES:
        - ""
        - "autocrypt"
        - "autocrypt,compression-bzip2"
        - "compression-bzip2"
  script:
    - cargo test --no-default-features --features crypto-nettle --features $FEATURES
  variables:
    CARGO_TARGET_DIR: /target
    CARGO_HOME: /cargo

bookworm-crypto-openssl:
  tags:
    - linux
  stage: build
  interruptible: true
  image: 192.168.122.1:5000/sequoia-pgp/build-docker-image/bookworm:latest
  dependencies:
    - codespell
  script:
    - cargo test --no-default-features --features crypto-openssl,compression-bzip2
  variables:
    CARGO_TARGET_DIR: /target
    CARGO_HOME: /cargo

all_commits:
  # Test each commit up to main, to facilitate bisecting.
  stage: test
  interruptible: true
  image: 192.168.122.1:5000/sequoia-pgp/build-docker-image/rust-stable:latest
  needs: ["rust-stable"]
  script:
    - .ci/all_commits.sh
  variables:
    CARGO_TARGET_DIR: /target
    CARGO_HOME: /cargo
    GIT_STRATEGY: clone

codespell:
  tags:
    - linux
  stage: pre-check
  interruptible: true
  image: 192.168.122.1:5000/sequoia-pgp/build-docker-image/bookworm:latest

  before_script:
    - *before_script_start
    - codespell --version
    - *before_script_end
  script:
    - codespell --summary -L "crate,ede,iff,mut,nd,te,uint,KeyServer,keyserver,Keyserver,keyservers,Keyservers,keypair,keypairs,KeyPair,fpr,dedup,deriver,certi,certp,certo" -S "*.bin,*.gpg,*.pgp,./.git,*/target,Cargo.lock"

deny:
  tags:
    - linux
  stage: test
  interruptible: true
  image: 192.168.122.1:5000/sequoia-pgp/build-docker-image/rust-stable:latest

  before_script:
    - *before_script_start
    - cargo install --locked cargo-deny
    - *before_script_end
  script:
    - cargo deny check

rust-stable:
  tags:
    - linux
  stage: build
  interruptible: true
  image: 192.168.122.1:5000/sequoia-pgp/build-docker-image/rust-stable:latest
  before_script:
    - *before_script_start
    - rustup override set stable
    - *before_script_end
  script:
    - cargo test --features=subplot
  variables:
    CARGO_TARGET_DIR: /target
    CARGO_HOME: /cargo

clippy:
  tags:
    - linux
  stage: build
  interruptible: true
  image: 192.168.122.1:5000/sequoia-pgp/build-docker-image/rust-stable:latest
  before_script:
    - *before_script_start
    - apt-get -y install libssl-dev capnproto libsqlite3-dev
    - rustup default 1.63.0
    - rustup component add clippy
    - cargo clippy --version
    - *before_script_end
  script:
    - cargo clippy
  variables:
    CARGO_TARGET_DIR: /target
    CARGO_HOME: /cargo

windows-gnu-cng:
  tags:
    - win
    - win2019
  stage: build
  interruptible: true
  image: 192.168.122.1:5000/sequoia-pgp/build-docker-image/windows-gnu
  only:
    variables:
      # Forks of this project most likely use gitlab's shared windows runners, which
      # do not use the docker executor, so disable the windows jobs for forks.
      - $CI_PROJECT_NAMESPACE == "sequoia-pgp"
  before_script:
    # We don't call *before_script_start or *before_script_end as we
    # don't have bash, clang, etc.
    - rustup default "1.63.0"
    - rustc --version --verbose
    - cargo --version
  script:
    # https://github.com/rust-lang/cargo/issues/5015
    - pacman --noconfirm --needed -S mingw-w64-x86_64-openssl
    - cargo test --tests --no-default-features --features crypto-cng,compression-bzip2,subplot
  after_script: [] # scriptlet doesn't work on Powershell
  variables:
    CFLAGS: "" # Silence some C warnings when compiling under Windows

docker-build-push:
  # Official docker image.
  image: docker:stable
  stage: build
  services:
    - docker:dind
  tags:
    - docker
    - self-hosted
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - >
      docker build
      --no-cache
      --target sq
      --tag "$IMAGE":latest
      .
    # smoke test
    - docker run "$IMAGE":latest --help
    - docker push "$IMAGE":latest
  after_script: []
  only:
    refs:
      - /docker/i # refs containing 'docker' keyword
      - tags
      - web
      - schedules
  variables:
    CI_REGISTRY: "registry.gitlab.com"
    IMAGE: "$CI_REGISTRY/sequoia-pgp/sequoia-sq"
    DOCKER_HOST: tcp://docker:2376

variables:
  DEBIAN_FRONTEND: noninteractive
  CARGO_HOME: $CI_PROJECT_DIR/../cargo
  CARGO_FLAGS: --color always
  CARGO_INCREMENTAL: 0
  RUST_BACKTRACE: full
  CFLAGS: -Werror
  QUICKCHECK_GENERATOR_SIZE: 500 # https://github.com/BurntSushi/quickcheck/pull/240
